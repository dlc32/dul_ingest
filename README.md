[[_TOC_]]
# DUL Drupal Data Ingest
Import Node, Taxonomy and other data -- from DUL's Drupal 7 instances -- into Drupal 9. 

## Dependencies
This proect depends on files (JSON, YAML) generated from a separate process.  
  
The current process responsible for generating the "ingest" data is found here:  
[Node-to-YAML Project](https://gitlab.oit.duke.edu/dlc32/node-to-yaml)

## Usage (requires Drush)
```sh
$ cd /path/to/drupal9/web

# create the DUL-specific content types
$ drush dul-ctypes

# import the file_managed entities
# this needs to be done before importing nodes
$ drush file-in /path/to/file_managed

# import node entities, specifying "content-type" where appropriate
$ drush node-in /path/to/node [--content-type=rubenstein_page|datagis_page|lilly_page|music_page]

# import taxonomy and terms
$ drush tax-in /path/to/taxonomy
```

## Goals
* Add the following content types -- to aide in content filtering:
  * `rubenstein_page`
  * `datavis_page`
  * `lilly_page`
  * `music_page`
* Create an "asidebox" block_content type, replacing the need for DUL's custom "asidebox" from its Drupal 7 instance.
* Import `file_managed` entries, mostly representing images attached to nodes and/or users.
* Import node content:
  * assigning appropriate content type
  * using UUIDs to preserve relationships with attached image files
  * creating "asidebox" block_content when present
  
## Content Types

**rubenstein_page** - Node content from [DUL's Rubenstein site](https://library.duke.edu/rubenstein).  
**datavis_page** - Node content from [DUL's Data & Visualization site](https://library.duke.edu/data).  
**lilly_page** - Node content from [DUL's Lilly Library site](https://library.duke.edu/lilly).  
**music_page** - Node content from [DUL's Music Library site](https://library.duke.edu/music).  

## Notes

### Asidebox Block Content
In DUL's Drupal 7 instance, "asidebox" content was tightly coupled with a node (page, article, etc).  
  
While this process correctly creates "asidebox" block_content, it will left to UI Content Editors to assign these widgets 
to appropriate content.
